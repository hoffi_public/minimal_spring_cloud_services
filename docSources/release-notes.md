Release notes
=============

## v1.0.0 - 2018-04-04

- Initial release

## v1.0.1 - 2018-04-13

- added tagging for Baggage in sending to zipkin
- added new child span for SourceTier1
