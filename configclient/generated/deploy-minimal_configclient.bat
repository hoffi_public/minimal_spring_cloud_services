@echo on
echo "Starting deployment of minimal_configclient"

cf login -a https://api.local.pcfdev.io -u admin -p "<password>" -o "pcfdev-org" -s "pcfdev-space" --skip-ssl-validation

cf push minimal_configclient -k 512M -m 768M -i 1 -p build\libs\minimal_configclient-0.1.0.NIGHTLY.jar --no-start
cf bind-service minimal_configclient RabbitMQ
cf bind-service minimal_configclient ConfigServer
cf set-env minimal_configclient VERSION "0.1.0.NIGHTLY"
cf set-env minimal_configclient BUILD_TIME "Sat Mar 10 00:54:00 CET 2018"
cf set-env minimal_configclient PUSHED_BY_USER "hoffmd9"
cf set-env minimal_configclient SPRING_PROFILES_ACTIVE "custom,zwei"
cf restart minimal_configclient
cf create-app-manifest minimal_configclient -p manifest-dev.yml
echo '  path: build\libs\minimal_configclient-0.1.0.NIGHTLY.jar' >> manifest-dev.yml

echo "Press key to close window"
pause
