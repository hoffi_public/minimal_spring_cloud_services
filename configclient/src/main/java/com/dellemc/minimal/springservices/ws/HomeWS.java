package com.dellemc.minimal.springservices.ws;

import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dellemc.minimal.springservices.bootconfigs.ConfigBean;
import com.dellemc.minimal.springservices.common.dto.HelloDTO;
import com.dellemc.minimal.springservices.helpers.RESThelper;

@RestController
@RequestMapping("/")
public class HomeWS {

    @Autowired
    private ConfigBean configBean;

    @GetMapping(produces = "application/json")
    public Map<RequestMethod, Set<String>> home() {
        return RESThelper.requestMappings("", new String[] { "", "WS" }, HomeWS.class.getPackage());
    }

    @GetMapping(value = { "/special/{customer}", "/special" }, produces = "text/plain")
    public String special(@PathVariable("customer") Optional<String> customer) {
        HelloDTO helloDTO = new HelloDTO();

        helloDTO.customer = customer.isPresent() ? customer.get() : helloDTO.customer;
        helloDTO.special = configBean.getDynamicProp();
        helloDTO.sauce = configBean.getStaticProp();

        return helloDTO.toString();
    }
}
