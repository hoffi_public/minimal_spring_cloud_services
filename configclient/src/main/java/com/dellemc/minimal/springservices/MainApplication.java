package com.dellemc.minimal.springservices;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.Cloud;
import org.springframework.cloud.service.ServiceInfo;
import org.springframework.cloud.service.common.MysqlServiceInfo;

@SpringBootApplication
public class MainApplication implements CommandLineRunner {

    @Value("${my.test.property}")
    String myTestProperty;

    @Autowired(required = false)
    Cloud cloud;

    public static void main(String[] args) {
        SpringApplication.run(MainApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        System.out.println("myTestProperty: " + myTestProperty);
        if (cloud != null) {
            List<ServiceInfo> infos = cloud.getServiceInfos();
            infos.stream().filter(it -> it instanceof MysqlServiceInfo)
                        .forEachOrdered(it -> System.out.println(it.getId() + ": " + ((MysqlServiceInfo) it).getJdbcUrl()));
            System.out.println("----------------------------------------------");
            System.out.println("cloudProperties:");
            System.out.println("----------------------------------------------");
            cloud.getCloudProperties().forEach((k, v) -> System.out.println(k + "=" + v));
            System.out.println("----------------------------------------------");
            System.out.println("cloud ServiceInfos:");
            System.out.println("----------------------------------------------");
            cloud.getServiceInfos().forEach(it -> System.out.println(it));
        } else {
            System.out.println("cloud == null");
        }
    }
}
