#!/bin/bash

echo "Starting deployment of minimal_configserver"

cf login -a https://api.local.pcfdev.io -u admin -p "<password>" -o "pcfdev-org" -s "pcfdev-space" --skip-ssl-validation

cf push minimal_configserver -k 256M -m 256M -i 1 -p build/libs/minimal_configserver-0.1.0.NIGHTLY.jar --no-start
cf set-env minimal_configserver VERSION "0.1.0.NIGHTLY"
cf set-env minimal_configserver BUILD_TIME "Sat Mar 10 00:54:05 CET 2018"
cf set-env minimal_configserver PUSHED_BY_USER "hoffmd9"
cf restart minimal_configserver
cf create-app-manifest minimal_configserver -p manifest-dev.yml
echo '  path: build/libs/minimal_configserver-0.1.0.NIGHTLY.jar' >> manifest-dev.yml