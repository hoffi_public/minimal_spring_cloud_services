package com.dellemc.minimal.circuitbreaker.ws;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.AutoConfigureJsonTesters;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.stubrunner.spring.AutoConfigureStubRunner;
import org.springframework.cloud.contract.stubrunner.spring.StubRunnerProperties;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import contracts.baseclasses.ws.TiersWSTestBaseClass;

// @formatter:off
@AutoConfigureMockMvc
@AutoConfigureJsonTesters
@AutoConfigureStubRunner(ids = "de.demo:minimal_circuitbreakers:+:stubs:9090",
                         stubsMode = StubRunnerProperties.StubsMode.LOCAL)
// @formatter:on
@TestPropertySource(properties = { "spring.application.name=circuitbreakers_tier1", "app.businessLogic.tier=tier1",
        "eureka.client.enabled=false", "spring.cloud.config.enabled=false", "management.endpoints.enabled-by-default=false",
        "management.endpoints.web.exposure.exclude=\"*\"" })
@SpringBootTest
public class TiersWSTest extends TiersWSTestBaseClass {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void tiersWS_returnsFailevery() throws Exception {
        //        String fooResourceUrl = "http://localhost:9090/sources";
        //        RestTemplate restTemplate = new RestTemplate();
        //
        //        ResponseEntity<String> responseEntity = restTemplate.getForEntity(String.format("%s%s", fooResourceUrl, "/sourcerate"),
        //                String.class);
        //        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        //        assertEquals("X  source rate now ms: 5000", responseEntity.getBody());

        MvcResult result = mockMvc.perform(get("/tiers/failevery").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
                .andExpect(content().string("X  will fail on every 5th call")).andReturn();

        String content = result.getResponse().getContentAsString();

    }

}
