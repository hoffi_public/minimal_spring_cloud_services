package com.dellemc.minimal.circuitbreaker.common.dto;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Disabled;

import annotations.TrivialTest;

class MessageDTOTest {

    @Disabled
    @TrivialTest
    void failTests() {}

    @TrivialTest
    void functionalTests() {
        MessageDTO m1 = new MessageDTO();
        assertEquals(Integer.valueOf(-1), m1.id);
        assertEquals("NONE", m1.message);
        assertEquals("", m1.modifiers);

        String bop = "functionalTests";
        String newMessage = "newMessage";
        String transformedBy = "TrivialTest";
        String instanceIndex = "42";
        MessageDTO m2 = m1.transform(bop, newMessage, transformedBy, instanceIndex);
        assertNotEquals(m1, m2);
        assertEquals(m2.id, m1.id);
        assertEquals(bop + newMessage, m2.message);
        assertTrue(m2.modifiers.startsWith(m1.modifiers));
        assertTrue(m2.modifiers.endsWith(" ==> " + transformedBy + ":i" + instanceIndex + ":" + bop));

        assertNotNull(m2.toString());
    }

}
