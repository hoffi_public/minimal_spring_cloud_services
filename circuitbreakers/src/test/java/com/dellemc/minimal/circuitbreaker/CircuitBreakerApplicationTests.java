package com.dellemc.minimal.circuitbreaker;

import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

//@RunWith(SpringRunner.class) // with spring-boot 2.1 the equivalent of this is included in @SpringBootTest annotation
@SpringBootTest
public class CircuitBreakerApplicationTests {

    @Test
    public void contextLoads() {}

}
