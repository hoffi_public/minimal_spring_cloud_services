package testhelpers;

import org.hamcrest.FeatureMatcher;
import org.hamcrest.Matcher;

import com.dellemc.minimal.circuitbreaker.common.dto.MessageDTO;

public abstract class DTOhelpers {
    // ========================================================================
    // ====   Test Helpers   ==================================================
    // ========================================================================
    public static FeatureMatcher<MessageDTO, Integer> id(Matcher<Integer> matcher) {
        return new FeatureMatcher<MessageDTO, Integer>(matcher, "has an id of", " is id") {
            @Override
            protected Integer featureValueOf(MessageDTO actual) {
                return actual.id;
            }
        };
    }

    public static FeatureMatcher<MessageDTO, String> message(Matcher<String> matcher) {
        return new FeatureMatcher<MessageDTO, String>(matcher, "has a message of", "is message") {
            @Override
            protected String featureValueOf(MessageDTO actual) {
                return actual.message;
            }
        };
    }

    public static FeatureMatcher<MessageDTO, String> modifiers(Matcher<String> matcher) {
        return new FeatureMatcher<MessageDTO, String>(matcher, "has modifiers of", "are modifiers") {
            @Override
            protected String featureValueOf(MessageDTO actual) {
                return actual.modifiers;
            }
        };
    }

}
