package contracts.baseclasses.ws;

import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.verifier.messaging.boot.AutoConfigureMessageVerifier;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.test.web.servlet.setup.StandaloneMockMvcBuilder;

import com.dellemc.minimal.circuitbreaker.ws.SourcesWS;

import io.restassured.module.mockmvc.RestAssuredMockMvc;

@ActiveProfiles({ "source" })
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@DirtiesContext
@AutoConfigureMessageVerifier
public abstract class SourcesWSTestBaseClass {

    @Autowired
    private SourcesWS sourcesWS;

    @BeforeEach
    public void setup() {
        StandaloneMockMvcBuilder standaloneMockMvcBuilder = MockMvcBuilders.standaloneSetup(sourcesWS);
        RestAssuredMockMvc.standaloneSetup(standaloneMockMvcBuilder);
    }
}
