package com.dellemc.minimal.circuitbreaker;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableCircuitBreaker
@EnableScheduling
@EnableDiscoveryClient
@SpringBootApplication
public class CircuitBreakerApplication implements CommandLineRunner {
    private static final Logger log = LoggerFactory.getLogger(CircuitBreakerApplication.class);

    @Value("${my.test.property}")
    String myTestProperty;

    public static void main(String[] args) {
        SpringApplication.run(CircuitBreakerApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        log.info("myTestProperty: {}", myTestProperty);
    }
}
