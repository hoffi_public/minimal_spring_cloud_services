//package com.dellemc.minimal.circuitbreaker.bootconfigs;
//
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.context.annotation.Profile;
//
//import com.dellemc.minimal.circuitbreaker.sourcebusinesslogic.BusinessLogic;
//
//@Configuration
//public class BusinessLogicConfig {
//
//    @Value("${app.info.instance_index}")
//    private String instanceIndex;
//    @Value("${app.businessLogic.sleepMin}")
//    public int sleepMin;
//    @Value("${app.businessLogic.sleepMax}")
//    public int sleepMax;
//
//    @Profile({ "source" }) // unused fake BusinessLogic for pure sources
//    @Configuration
//    class BusinessLogicSourceConfig {
//        @Bean
//        public BusinessLogic businessLogic() {
//            return new BusinessLogic("source", instanceIndex, sleepMin, sleepMax);
//        }
//    }
//
//    @Profile({ "sink" }) // unused fake BusinessLogic for pure sinks
//    @Configuration
//    class BusinessLogicSinkConfig {
//        @Bean
//        public BusinessLogic businessLogic() {
//            return new BusinessLogic("sink", instanceIndex, sleepMin, sleepMax);
//        }
//    }
//
//    @Profile({ "tier1" })
//    @Configuration
//    class BusinessLogicTier1Config {
//        @Bean
//        public BusinessLogic businessLogic() {
//            return new BusinessLogic("tier1", instanceIndex, sleepMin, sleepMax);
//        }
//    }
//
//    @Profile({ "tier2" })
//    @Configuration
//    class BusinessLogicTier2Config {
//        @Bean
//        public BusinessLogic businessLogic() {
//            return new BusinessLogic("tier2", instanceIndex, sleepMin, sleepMax);
//        }
//    }
//
//    @Profile({ "tier3" })
//    @Configuration
//    class BusinessLogicTier3Config {
//        @Bean
//        public BusinessLogic businessLogic() {
//            return new BusinessLogic("tier3", instanceIndex, sleepMin, sleepMax);
//        }
//    }
//
//}
